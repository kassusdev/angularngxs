// File name app.component.ts
import { Component } from "@angular/core";
import { Store, Select } from "@ngxs/store";
import {
  AddTodo,
  EditTodo,
  FetchAllTodos,
  DeleteTodo,
  markDone,
} from "./actions/todo-actions";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { TodoState, ITodo } from "../app/states/todo-state";
import { Observable } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "angularngxs";
  constructor(private store: Store) {}
  @Select(TodoState)
  todoList$!: Observable<ITodo>;

  addForm = new FormGroup({
    title: new FormControl("", [Validators.required]),
  });

  onSubmit(form: any) {
    this.store.dispatch(new AddTodo(form));
  }
}
