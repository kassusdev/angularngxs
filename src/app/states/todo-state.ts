// File name todo-state.ts
import { Injectable } from "@angular/core";
import { State, NgxsOnInit, Action, StateContext } from "@ngxs/store";
import {
  AddTodo,
  EditTodo,
  FetchAllTodos,
  DeleteTodo,
  markDone,
} from "../actions/todo-actions";
import { Store } from "@ngxs/store";
import { patch, updateItem } from "@ngxs/store/operators";

export interface ITodo {
  id: number;
  title: string;
  is_done: boolean;
}
export interface ITodoStateModel {
  todoList: ITodo[];
}
@State<ITodoStateModel>({
  name: "todoList",
  defaults: {
    todoList: [],
  },
})
@Injectable()
export class TodoState implements NgxsOnInit {
  ngxsOnInit(ctx: any) {
    ctx.dispatch(FetchAllTodos);
  }

  @Action(AddTodo)
  add(ctx: StateContext<ITodoStateModel>, { payload }: AddTodo) {
    const state = ctx.getState();
    ctx.setState({
      ...state,
      todoList: [
        ...state.todoList,
        {
          ...payload,
          id: Math.random().toString(36).substring(7),
          is_done: false,
        },
      ],
    });
  }
}
